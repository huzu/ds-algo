def merge(left_array, right_array)
  sorted_array = []

  # If either array is empty we don't need to compare them
  while !left_array.empty? && !right_array.empty? do
    # we are shifting out the compared/used values so we don't repeat

    if left_array[0] < right_array[0]
      sorted_array.push(left_array.shift)
    else
      sorted_array.push(right_array.shift)
    end
  end

  #concat appends elements of another array to an array
  return sorted_array.concat(left_array).concat(right_array)
end

def merge_sort(unsorted_array)
  # if array only has one element or fewer there is nothing to do
  if unsorted_array.length <=1
    return unsorted_array
  else
    # dividing and then merge-sorting the halves
    mid = unsorted_array.length/2

    # this merge sort call will recursively complete it's execution - return an output in first_half - after this only it will proceed to the original 2nd half of the array.

    #For ex: [5, 2, 6, 1] here first half will be [5, 2] which will given as input to below merge sort method which recursively break it to [5] and [2] and then will be passed to merge method which sort and return sorted result as [2, 5]

    first_half = merge_sort(unsorted_array.slice(0...mid))

    # Now as per the rule of recursion, code execution will forward to below code with input as [6, 1] and same as above - it will too recursively break this down to [6] and [1] - will be passed to merge which will return a sorted result as [1,6]
    second_half = merge_sort(unsorted_array.slice(mid...unsorted_array.length))

    # we now have both original first and second half sorted as [2, 5] and [1, 6] of the  original unsorted array [5, 2, 6, 1].
    # This will now be passed to merge method for the final sorted result as [1, 2, 5, 6]
    merge(first_half, second_half)
  end
end


# to test above code run below
unsorted_array = [5, 2, 6, 1]

# call the merge sort method to return the sorted array
merge_sort(unsorted_array)