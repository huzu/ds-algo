=begin
You are given an integer array nums and you have to return a new counts array. The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].

Example 1:

Input: nums = [5,2,6,1]
Output: [2,1,1,0]
Explanation:
To the right of 5 there are 2 smaller elements (2 and 1).
To the right of 2 there is only 1 smaller element (1).
To the right of 6 there is 1 smaller element (1).
To the right of 1 there is 0 smaller element.
=end


def count_smaller(nums)
  inversion_map = {}

  # initialize each element of the array with key as itself and value as 0
  nums.each_with_index { |v, index| inversion_map[nums[index]] = 0 }

  # execute the merge sort algorithm and count the inversions for element smaller than self on the right side.
  merge_sort(nums, inversion_map)

  # initialize an array of same length of the original array which will be used to store the inversion counts of each element as per the problem statement.
  count_arr = Array.new(nums.length, 0)

  # replace the count_arr with the inversion counts from the inversion map hash.
  inversion_map.each do |num, inv_count|
    arr_index_to_replace = nums.find_index(num)
    count_arr[arr_index_to_replace] = inv_count
  end

  # this will return the expected result of problem statement.
  return count_arr
end

def merge_sort(unsorted_array, inversion_map)
  # if array only has one element or fewer there is nothing to do
  if unsorted_array.length <=1
    return unsorted_array
  else
    # dividing and then merge-sorting the halves
    mid = unsorted_array.length/2
    first_half = merge_sort(unsorted_array.slice(0...mid), inversion_map)
    second_half = merge_sort(unsorted_array.slice(mid...unsorted_array.length), inversion_map)
    merge(first_half, second_half, inversion_map)
  end
end

def merge(left_array, right_array, inversion_map)
  sorted_array = []
  # If either array is empty we don't need to compare them
  while !left_array.empty? && !right_array.empty? do

    # this is the only additional block of code in the original merge sort algorithm.
    # this check if the left array value is higher than the right - here we know that all the values on the right hand side of this left array index will be more than the right array value.
    if left_array[0] > right_array[0]
      inversion_map[left_array[0]] += 1

      # ex [2, 5] and [1,6] -- here 2 > 1 - hence all the elements after 2 will also be more than 1 - hence we need to increment inversion count of 2 and all the elements after it.
      left_array[1..-1].each do |val|
        inversion_map[val] += 1
      end
    end

    # we are shifting out the compared/used values so we don't repeat
    if left_array[0] < right_array[0]
      sorted_array.push(left_array.shift)
    else
      sorted_array.push(right_array.shift)
    end
  end

  #concat appends elements of another array to an array
  return sorted_array.concat(left_array).concat(right_array)
end


# check the above code by executing below
nums = [5, 2, 6, 1]
count_smaller(nums)
